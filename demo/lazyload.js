var lazyLoad = (function($) {
  return function() {
    // only target divs with non empty lazy src attribute
    $('div[data-lazy-src!=""][data-lazy-src]').each(function() {
      var watcher = scrollMonitor.create(this, 150);

      // add reveal event that only fires once
      watcher.one('enterViewport', function() {
        var $el = $(this.watchItem);

        // destroy the watcher
        this.destroy();

        // loading
        $el.addClass('loading');

        // send of as anonymous function to handle multiple images
        (function($el) {
          var src = $el.data('lazy-src');

          // reveal child div when image is loaded, via loaded class
          $('<img />')
            .on('load', function() {
              $el
                .addClass('loaded')
                .removeClass('loading');

              // append child div with background image
              $el.append('<div style="background-image: url(' + src + ')" />');

              // if svg and padding bottom is set inline
              if ((""+$el.attr('style')).indexOf('padding-bottom') !== -1) {
                if (src.match(/\.(svg)$/) != null) {
                  $el.css('padding-bottom', (this.naturalHeight / this.naturalWidth * 100) + '%');
                }
              }
            })
            .attr('src', src);
        })($el);
      });
    });
  };
})(jQuery);