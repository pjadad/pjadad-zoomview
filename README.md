# PJADAD ZoomView

## TODO
- Handle videos as well?

## Issues

When you click on an image thumbnail, there's a qick flicker. Could be a good idea to check if the image is already cached, and 'load' the image differently then. (https://stackoverflow.com/questions/15352803/how-to-check-if-an-image-was-cached-in-js)

Zoom-in and Zoom-out cursors are not supported by IE